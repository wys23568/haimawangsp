const service = require("../../api/server.js");
Page({
  data: {
    scrollTop: 0,
    arriveBottom: false,
    info: {},
  },
  onLoad: function (options) {
    service.courseQrCode({ courseId: options.courseId}, res=> {
      this.setData({
        info: res.data,
      })
    })
  },
  // 下滑
  scrollDown: function () {
    this.setData({
      scrollTop: 1000,
      arriveBottom: true,
    })
  },
  // 上滑
  scrollup: function () {
    this.setData({
      scrollTop: 0,
      arriveBottom: false,
    })
  },
  // 滑动到顶部
  upper: function () {
    this.setData({
      arriveBottom: false,
    })
  },
  // 滑动到底部
  lower: function () {
    this.setData({
      arriveBottom: true,
    })
  },
  previewImg: function (e) {
    console.log(e.currentTarget.dataset.src);
    wx.previewImage({
      current: e.currentTarget.dataset.src,
      urls: [e.currentTarget.dataset.src],
    })
  },
})