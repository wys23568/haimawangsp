const app = getApp()
const service = require("../../api/server.js");
import { onGotUserInfo } from "../../utils/util.js";

Page({
  data: {
    motto: 'Hello World',
    needBindTel: false,
    showTimes: false,
    times: {},
    loginstate: "checking",
    buyFlag: 0,
    curTimes: 0,   // 当前选择期次的index
  },
  onShow: function () {
    this.setData({
      showTimes: false,
      loginstate: app.globalData.loginstate,
      needBindTel: wx.getStorageSync("loginToken") ? false : true, // 没有loginToken 没绑定 需要绑定手机号码
    })
    
    this.request();
  },
  request: function () {
    service.getCoursePackage(res => {
      this.setData({
        times: res.data[0],
        buyFlag: res.data[0].courseInfoList[0].buyFlag,
      })
    })
  },
  onLoad: function () {},
  // 手机号码授权
  onGotUserPhone: function (res) {
    let credential = res.detail.errMsg.match('ok');
    if (credential) {
      service.bindPhone(res.detail, resArg => {
        wx.setStorageSync("loginToken", resArg.data.loginToken);
        this.setData({
          needBindTel: false,
        })
        this.showTimes();
        var params = {
          loginToken: wx.getStorageSync("loginToken"),
          name: wx.getStorageSync("userInfo").nickName,
          gender: wx.getStorageSync("userInfo").gender,
          avatarUrl: wx.getStorageSync("userInfo").avatarUrl,
          useFlag: 2,
        }
        service.getUserInfo(params, res => {})
      })
    }
  },
  showTimes: function () {
    this.setData({
      showTimes: !this.data.showTimes,
    })
  },
  // 选择课程期次
  chooseTimes: function (e) {
    this.setData({
      curTimes: e.currentTarget.dataset.index
    })
  },
  // 确认选课
  confirmPay: function () {
    var courseId = this.data.times.courseInfoList[this.data.curTimes].id;
    service.createOrder({ courseId }, res=> {
      if (RegExp("requestPayment:ok").test(res.errMsg)) {
        wx.showToast({
          title: '支付成功',
        })
        this.setData({
          showTimes: false,
        })
        this.request();
        wx.navigateTo({
          url: `../payResult/payResult?status=suc&courseId=${courseId}&orderNo=${res.orderNo}`,
        })
      }else if(RegExp("fail cancel").test(res.errMsg)) {
        this.setData({
          showTimes: false,
        })
        wx.showLoading({
          title: '支付取消',
        })
        setTimeout(function () {
          wx.navigateTo({
            url: '../center/order/order',
          })
        }, 1500)
      } else {
        wx.showToast({
          icon: 'none',
          title: res.errMsg,
        })
        wx.navigateTo({
          url: `../payResult/payResult?status=fail&courseId=${courseId}&orderNo=${res.orderNo}`,
        })
      }
    })
  },
  onShareAppMessage: function () {
    return {
      title: '属于孩子的编程课，让创造多一种形式',
      imageUrl: '../../img/shareCon.png',
      path: '/pages/index/index',
      success: function () {
        wx.showToast({
          title: '分享成功',
        })
      }
    }
  },
  onHide: function () {
    this.setData({
      showTimes: false,
    })
  },
  onGotUserInfo: function (res) {
    const result = onGotUserInfo(res);
    if (result) {
      wx.showLoading({
        title: '登录中',
      })
      app.globalData.loginstate = "hasLogin";
      this.setData({
        loginstate: "hasLogin",
      })
      var that = this;
      setTimeout(res => {
        wx.hideLoading();
        that.setData({
          needBindTel: wx.getStorageSync("loginToken") ? false : true,
        })
      },1000)
    }
  },
  onUnload: function () {
    this.setData({
      buyFlag: 0,
    })
  },
})
