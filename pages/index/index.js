
const service = require("../../api/server.js");
const app = getApp()
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    loginstate: "checking",
  },
  onLoad: function () {
    wx.showLoading({ title: '登录中...', })
    wx.login({
      success(res) {
        wx.setStorageSync('code', res.code)
      },
      fail(err) {}
    })
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo'] && wx.getStorageSync("loginStatus") != 402) {
          app.globalData.loginstate = "hasLogin";
        } else {
          app.globalData.loginstate = "needLogin";
          wx.hideLoading();
        }
        wx.reLaunch({
          url: '../learning/learnList/learnList',
        })
      },
      fail: err => {
        console.log(err)
      }
    })
  },
  onGotUserInfo: function (res) {
    wx.showLoading({ title: '登录中...', })
    var that = this;
    let credential = res.detail.errMsg.match('ok');
    // 授权
    if (credential) {
      app.globalData.scopeUser = true;

      wx.getUserInfo({
        success: res => {
          service.codeLogin(res, loginRes => {
            if (loginRes.data.loginToken) {
              wx.setStorageSync("loginToken", loginRes.data.loginToken)
              wx.setStorageSync("loginStatus", 200)
              wx.reLaunch({
                url: '../learning/learnList/learnList',
              })
              var params = {
                loginToken: loginRes.data.loginToken,
                name: res.userInfo.nickName,
                gender: res.userInfo.gender,
                avatarUrl: res.userInfo.avatarUrl,
                useFlag: 1,
              }
              service.getUserInfo(params, res => { })
            } else {
              wx.setStorageSync("loginInfo", loginRes.data)
              wx.reLaunch({
                url: '../buyCourse/buyCourse',
              })
            }
          })
          wx.setStorageSync('userInfo', res.userInfo);
        }
      })
    }
    // 拒绝授权
    if (!credential) {
      wx.showToast({
        title: '授权后方可使用小程序哦~',
        icon: 'none'
      });
    }
  },
})
