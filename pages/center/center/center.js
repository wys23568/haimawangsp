const service = require("../../../api/server.js");
import { onGotUserInfo } from "../../../utils/util.js";
const app = getApp()
Page({
  data: {
    info: {},
    loginTokenisNull: false,
    wxUserInfo: {},
    loginstate: "checking",
  },
  onShow: function () {
    this.setData({
      loginstate: app.globalData.loginstate,
    })
    this.init();
  },
  onLoad: function (options) { },
  init: function () {
    this.setData({
      loginTokenisNull: !wx.getStorageSync("loginToken"),
      wxUserInfo: !wx.getStorageSync("loginToken") ? wx.getStorageSync("userInfo") : {},
    })
    if (!wx.getStorageSync("loginToken")) {
      return false;
    }
    service.getUserInfo({"name": '', "avatar": "",useFlag: 1, }, res => {
      wx.setStorageSync("serverInfo", res.data)
      this.setData({
        info: res.data
      })
      if (res.data.avatarUrl == null) {
        this.setData({
          wxUserInfo: wx.getStorageSync("userInfo"),
        })
      }
    })
  },
  toPage: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.src,
    })
  },
  // 登录授权
  onGotUserInfo: function (res) {
    const result = onGotUserInfo(res);
    if (result) {
      wx.showLoading({
        title: '登录中',
      })
      app.globalData.loginstate = "hasLogin";
      this.setData({
        loginstate: "hasLogin",
      })
      var that = this;
      setTimeout(res => {
        wx.hideLoading();
        that.init();
      }, 1000)
    }
  },
  // 手机号码授权
  onGotUserPhone: function (res) {
    let credential = res.detail.errMsg.match('ok');
    if (credential) {
      service.bindPhone(res.detail, resArg => {
        wx.setStorageSync("loginToken", resArg.data.loginToken);
        var params = {
          loginToken: wx.getStorageSync("loginToken"),
          name: wx.getStorageSync("userInfo").nickName,
          gender: wx.getStorageSync("userInfo").gender,
          avatarUrl: wx.getStorageSync("userInfo").avatarUrl,
        }
        this.init();
      })
    }
  },
  onShareAppMessage: function () {
    return {
      title: '属于孩子的编程课，让创造多一种形式',
      imageUrl: '../../../img/shareCon.png',
      path: '/pages/index/index',
      success: function () {
        wx.showToast({
          title: '分享成功',
        })
      }
    }
  }
})