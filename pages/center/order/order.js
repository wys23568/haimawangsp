const service = require("../../../api/server.js");
Page({
  data: {
    orderList: [],
    loadAll: false,
    curPage: 0,
  },
  onLoad: function (options) {
    this.request();
  },
  request: function () {
    service.orderList({ pageNumber: this.data.curPage }, res => {
      if ((res.data.pageable.pageNumber + 1) * res.data.pageable.pageSize >= res.data.pageable.totalSize) {
        this.setData({
          loadAll: true,
        })
      }
      var orderList = this.data.orderList.concat(res.data.content);
      this.setData({
        orderList: orderList,
      })
    })
  },
  loadMore: function () {
    if(!this.data.loadAll) {
      var curPage = this.data.curPage + 1;
      this.setData({
        curPage: curPage
      })
      this.request();
    }
  },
  toDetail: function (e) {
    wx.navigateTo({
      url: `../order/detail?orderno=${e.currentTarget.dataset.orderno}`,
    })
  },
  // 立即支付
  repay: function (e) {
    var courseId = e.currentTarget.dataset.courseid;
    service.createOrder({ courseId }, res => {
      console.log("支付结果",res)
      if (RegExp("requestPayment:ok").test(res.errMsg)) {
        wx.showToast({
          title: '支付成功',
        })
        wx.navigateTo({
          url: `../../payResult/payResult?status=suc&courseId=${courseId}&orderNo=${res.orderNo}`,
        })
        this.setData({
          curPage: 0,
          orderList: [],
        })
        this.request();
      } else if (RegExp("fail cancel").test(res.errMsg)) {
        wx.showLoading({
          title: '支付取消'
        })
        setTimeout(()=> {
          wx.hideLoading();
        },500)
      } else {
        wx.showToast({
          icon: 'none',
          title: res.errMsg,
        })
        wx.navigateTo({
          url: `../../payResult/payResult?status=fail&courseId=${courseId}`,
        })
      }
    })
  }
})