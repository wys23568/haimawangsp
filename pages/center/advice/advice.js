const server = require("../../../api/server.js");
Page({
  data: {
    content: '',
    contact: '',
  },
  onLoad: function (options) {

  },
  inputCon: function (e) {
    console.log(e);
    this.setData({
      content: e.detail.value,
    })
  },
  inputTel: function (e) {
    console.log(e)
    this.setData({
      contact: e.detail.value,
    })
  },
  commentPush: function () {
    if(this.data.content.length < 20) {
      
    }
    server.commentPush({"content": this.data.content, contact: this.data.contact},res=> {
      wx.showToast({
        title: '提交成功',
      })
      setTimeout(function () {
        wx.navigateBack({})
      },1500)
    })
  }
})