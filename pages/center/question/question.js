Page({
  data: {
    showQ1: true,
    showQ2: false,
    showQ3: false,
    showQ4: false,
    showQ5: false,
    showQ6: false,
    showQ7: false,
  },
  onLoad: function (options) {

  },
  showCon: function (e) {
    console.log(e.currentTarget.dataset);
    console.log(!this.data["showQ" + e.currentTarget.dataset.qindex]);
    this.setData({
      ["showQ" + e.currentTarget.dataset.qindex]: !this.data["showQ" + e.currentTarget.dataset.qindex]
    })
  },
})