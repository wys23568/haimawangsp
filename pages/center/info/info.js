const server = require("../../../api/server.js");
var config = require('../../../config.js');

Page({
  data: {
    info: {},
    userName: '',
    avatarUrl: '',
    canvasSize: 400,
    wxUserInfo: {},
  },
  onLoad: function (options) {
    this.setData({
      wxUserInfo: wx.getStorageSync("userInfo"),
    })
    server.getUserInfo({
      "name": '', "avatar": "",useFlag: 1, }, res => {
      this.setData({
        info: res.data
      })
      if(res.data.gender == null) {
        this.setData({
          ["info.gender"]: this.data.wxUserInfo.gender
        })
      }
    })
  },
  inputName: function (e) {
    console.log(e)
    this.setData({
      userName: e.detail.value
    })
  },
  chooseGender: function  (e) {
    this.setData({
      ["info.gender"]: e.currentTarget.dataset.gender
    })
  },
  chooseImg: function () {
    var this_ = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        const imgSrc = res.tempFilePaths[0];
        wx.getImageInfo({
          src: imgSrc,
          success: resInfo => {
            const ctx = wx.createCanvasContext('canvasBody')
            var imgType = resInfo.width / resInfo.height;
            var imgSize, leftX, leftY;
            if (imgType >= 1) {
              imgSize = resInfo.height; // 以小边为图片长宽尺寸
              leftX = (resInfo.width - resInfo.height) / 2;
              leftY = 0;
            } else {
              imgSize = resInfo.width;
              leftX = 0;
              leftY = (resInfo.height - resInfo.width) / 2
            }
            this_.setData({
              canvasSize: imgSize
            })
            ctx.drawImage(imgSrc, leftX, leftY, imgSize, imgSize, 0, 0, imgSize, imgSize);
            wx.showLoading({
              title: '上传中……',
            })
            ctx.draw(true, setTimeout(function () {
              wx.canvasToTempFilePath({
                x: 0,
                y: 0,
                width: imgSize,
                height: imgSize,
                destWidth: imgSize,
                destHeight: imgSize,
                canvasId: 'canvasBody',
                success(resd) {
                  wx.uploadFile({
                    header: {
                      'Content-Type': 'application/json'
                    },
                    // url: 'https://www.haimacode.com/test/api/haimawang/account/student/uploadFile',  // 开发版
                    // url: 'https://www.haimacode.com/api/haimawang/account/student/uploadFile',  // 线上版
                    url: config.host + config.api.uploadFile,
                    filePath: resd.tempFilePath,
                    name: 'upload',
                    formData: {
                      loginToken: wx.getStorageSync("loginToken"),
                    },
                    success(result) {
                      if (JSON.parse(result.data).code == 100) {
                        wx.hideLoading();
                        wx.showToast({
                          icon: 'none',
                          title: JSON.parse(result.data).error,
                        })
                        return false;
                      }
                      this_.setData({
                        ["info.avatarUrl"]: JSON.parse(result.data).data.url,
                      })
                      wx.hideLoading();
                    }
                  })
                }
              })
            }, 1000));
          }
        })
      },
      fail: function (err) {
        console.warn(err);
      }
    })
  },
  submitFn: function () {
    var params = {
      loginToken: wx.getStorageSync("loginToken"),
      name: this.data.userName != "" ? this.data.userName : this.data.info.name,
      gender: this.data.info.gender,
      // avatarUrl: this.data.info.avatarUrl == null ? this.data.wxUserInfo.avatarUrl : this.data.info.avatarUrl,
      avatarUrl: this.data.info.avatarUrl || this.data.wxUserInfo.avatarUrl,
    }
    server.updateInfo(params,res=> {
      wx.showToast({
        title: '修改成功',
      })
      setTimeout(function () {
        wx.navigateBack({})
      },1500)
    })
  },
})