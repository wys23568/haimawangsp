const service = require("../../../api/server.js");
Page({
  data: {
    phone: '',
    hasBindAccount: true,
  },
  onShow: function () {
    this.init();
  },
  onLoad: function (options) {},
  init: function () {
    if (wx.getStorageSync("loginToken") && wx.getStorageSync("loginToken") !== "") {
      this.setData({
        hasBindAccount: true
      })
    } else {
      this.setData({
        hasBindAccount: false
      })
      return false;
    }
    this.setData({
      phone: wx.getStorageSync("serverInfo").phone
    })
  },
  // 手机号码授权
  onGotUserPhone: function (res) {
    let credential = res.detail.errMsg.match('ok');
    if (credential) {
      service.bindPhone(res.detail, resArg => {
        wx.setStorageSync("loginToken", resArg.data.loginToken);
        var params = {
          loginToken: wx.getStorageSync("loginToken"),
          name: wx.getStorageSync("userInfo").nickName,
          gender: wx.getStorageSync("userInfo").gender,
          avatarUrl: wx.getStorageSync("userInfo").avatarUrl,
          useFlag: 2,
        }
        
        service.getUserInfo(params, res => {
          wx.setStorageSync("serverInfo", res.data);
          this.setData({
            hasBindAccount: true,
            phone: res.data.phone
          })
        })
      })
    }
  },
  changePhone: function () {
    wx.navigateTo({
      url: '../unbindPhone/unbindPhone',
    })
  },
})