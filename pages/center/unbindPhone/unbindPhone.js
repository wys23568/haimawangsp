const server = require("../../../api/server.js");
var timer = null;
Page({
  data: {
    phone: "",
    code: "",
    hasSendCode: false,
    seconds: 60,
    showTips: false,
    waiting: false,
  },
  onLoad: function (options) {
    this.setData({
      phone: wx.getStorageSync("serverInfo").phone
    })
  },
  phoneFn: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  getCode: function () {
    server.unbindTel(res => {
      if (res.data.validity >= 60) {
        this.setData({
          hasSendCode: true,
        })
        wx.showToast({
          title: '验证码已发送',
        })
      } else {
        this.setData({
          waiting: true,
          showTips: true,
        })
      }
      this.timeout();
    })
  },
  // 倒计时
  timeout: function () {
    var this_ = this;
    timer = setInterval(function () {
      if (this_.data.seconds <= 1) {
        this_.setData({
          hasSendCode: false,
          seconds: 60,
          showTips: false,
          waiting: false,
        })
        clearInterval(timer);
      }
      var curTime = this_.data.seconds - 1;
      this_.setData({
        seconds: curTime
      })
    }, 1000)
  },
  codeFn: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  nextStep: function () {
    console.log(this.data.phone)
    server.unbindTelCode({ verifyCode: this.data.code}, res=> {
      wx.redirectTo({
        url: '../bindPhone/bindPhone',
      })
    })
  },
  onUnload: function () {
    clearInterval(timer);
  }
  
})