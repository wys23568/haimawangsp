const app = getApp();
const service = require("../api/server.js");

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const onGotUserInfo = (res) => {
  wx.showLoading({ title: '登录中...', })
  var that = this;
  let credential = res.detail.errMsg.match('ok');
  // 授权
  if (credential) {
    app.globalData.loginstate = "hasLogin";
    app.globalData.scopeUser = true;
    wx.getUserInfo({
      success: res => {
        service.codeLogin(res, loginRes => {
          if (loginRes.data.loginToken) {
            wx.setStorageSync("loginToken", loginRes.data.loginToken)
            wx.setStorageSync("loginStatus", 200);
            var params = {
              loginToken: loginRes.data.loginToken,
              name: res.userInfo.nickName,
              gender: res.userInfo.gender,
              avatarUrl: res.userInfo.avatarUrl,
              useFlag: 1,
            }
            service.getUserInfo(params, res => { })
          } else {
            wx.setStorageSync("loginInfo", loginRes.data)
          }
        })
        wx.setStorageSync('userInfo', res.userInfo);
      }
    })
  }
  // 拒绝授权
  if (!credential) {
    wx.showToast({
      title: '授权后方可享受更多服务哦~',
      icon: 'none'
    });
  }
  return credential;
}
module.exports = {
  formatTime: formatTime,
  onGotUserInfo,
}