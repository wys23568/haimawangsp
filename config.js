var develop = "https://www.haimacode.cn/test/api"; // 测试

var product = "https://www.haimacode.cn/api";    // 线上
// 旧版本 wx1c87c80d569cdd8b

var release = "";

var host = product;

const api = {
  codeLogin: '/service/account/common/sso/wxxcx/login',
  codePhone: '/service/account/common/sso/wxxcx/phone',   // 手机号授权-获取手机号码
  bindPhone: '/service/account/common/sso/wxxcx/phone/binding',   // 手机号授权-获取手机号码
  unbindTel: '/service/account/common/phone/unbinding/request',   // 解绑手机号验证码
  unbindTelCode: '/service/account/common/phone/unbinding/confirm',   // 解绑手机号
  bindTelCode: '/service/account/common/phone/binding/request',   // 获取绑定手机号验证码
  bindTel: '/service/account/common/phone/binding/confirm',   // 绑定手机号

  
  
  coursePackage: '/haimawang/course/sign4Package',    //课程购买页，可购买课程 期次
  courseMoney: '/haimawang/account/student/course/sign',   // 课程购买页_课程金额
  courseList: '/haimawang/account/student/course/list',   // 正在学习页_列表
  courseDetail: '/haimawang/account/student/course/detail',   // 正在学习页_列表详情
  courseQrCode: '/haimawang/account/student/course/order/finish',   // 二维码获取
  info: '/haimawang/account/student/info',   // 获取个人信息
  update: '/haimawang/account/student/update',   // 更新信息
  commentPush: '/haimawang/account/student/comment/commit',   // 意见反馈
  createOrder: '/haimawang/account/student/course/order/create',   // 创建订单
  orderList: '/haimawang/account/student/course/order/list',   // 订单列表
  orderDetail: '/haimawang/account/student/course/order/detail',   // 订单列表详情
  uploadFile: '/haimawang/account/student/uploadFile',   // 上传

  
}



var config = {
  host,
  api,
}
module.exports = config;