const app = getApp();
function apiHeader() {
  return {
    'content-type': 'application/json',
    // 'cookie': "koa.sid=" + wx.getStorageSync("loginCredentials")
  }
}
function result(res, callback) {
  if (res.code == "200") {
    wx.setStorageSync("loginStatus", 200)
    return callback && callback(res);
  } else if (res.code == '402') {
    wx.setStorageSync("loginStatus", 402)
    wx.showToast({
      title: '请重新登录',
    })
    wx.clearStorageSync("loginToken");
    app.globalData.loginstate = "needLogin";
    setTimeout(function () {
      wx.reLaunch({
        url: '/pages/index/index',
      })
    }, 1000)
  } else {
    wx.setStorageSync("loginStatus", 200)
    wx.hideLoading();
    wx.showToast({
      icon: 'none',
      title: res.error,
    })
  }
}


module.exports = {
  apiHeader,
  result,
}